package net.nnemesis.forest;

/**
 * Created by Nicholas on 2015-10-25.
 */
public interface Client {
    public abstract void sendMessage(String msg, boolean important);
    public abstract void addInputListener(InputListener list);

    public abstract void disconnect(boolean force);

    public abstract int getID();
}
